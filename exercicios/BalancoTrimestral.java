class BalancoTrimestral {
    public static void main(String[] args){
        int gastoJaneiro = 15000;
        int gastoFevereiro = 23000;
        int gastoMarco = 17000;
        int gastosTrimestre = (gastoJaneiro + gastoFevereiro + gastoMarco);
        int mediaMensal = gastosTrimestre / 3;

        System.out.println("Gasto trimestral: R$" + gastosTrimestre);
        System.out.println("Valor da média mensal: R$" + mediaMensal);
    }
}
