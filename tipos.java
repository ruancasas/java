class Tipos {
    public static void main(String[] args){
        int idadeJoao = 18;
        int idadeMaria = 21;

        int somaDasIdades = idadeJoao + idadeMaria;

        System.out.println(somaDasIdades);

        double pi = 3.1415;
        double dobroPi = pi * 2;
        float outroPi = 3,14;

        System.out.println(dobroPi);

        boolean amigo = true;
        boolean inimigo = !amigo;

        boolean maiorDeIdade = idadeJoao <= 18;

        char letra = 'M';
        String nomeCompleto = "João da Silva";
        System.out.println(nomeCompleto);

        long numeroGrande = 99999999999L;

        long numero = 614;
        int copiaDeNumero = (int) numero;
    }
}
