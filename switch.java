class Switch {
    public static void main(String[] args){
        int num = 1;

        switch(num) {
            case 1:
                System.out.println("Um");
                break;
            case 2:
                System.out.println("Dois");
                break;
            case 3:
                System.out.println("Tres");
                break;
            default:
                System.out.println("Numero grande");
        }
    }
}
